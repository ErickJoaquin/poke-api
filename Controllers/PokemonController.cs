using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;

namespace aspApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PokemonController : ControllerBase
    {
        public IHttpClientFactory fac;
        public PokemonController([FromServices] IHttpClientFactory factory){
            fac = factory;
        }
        // GET: api/Pokemon
        [HttpGet]
        public Task<string> Get()
        {
            var client = fac.CreateClient();
            client.BaseAddress = new Uri("https://pokeapi.co");
            return client.GetStringAsync($"/api/v2/pokemon");
        }

        // GET: api/Pokemon/5
        [HttpGet("{pokemon}", Name = "Get")]
        public Task<string> Get(string pokemon)
        {
            var client = fac.CreateClient();
            client.BaseAddress = new Uri("https://pokeapi.co");
            string url = "/api/v2/pokemon/" + pokemon;
            return client.GetStringAsync(url);
        }
    }
}
